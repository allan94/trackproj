"""mysite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from views import data
from views import tracks
from views import loadTracks
from views import turnOnpulsePin
from views import togglePin
from views import trackInput
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from views import gpioSetup

gpioSetup()

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^data', data),
    url(r'^tracks', tracks),
    url(r'^loadTrack', loadTracks),
    url(r'^trackInput', trackInput, name="trackInput"),
    url(r'^pulseOn', turnOnpulsePin),
    url(r'^togglePin', togglePin),
]
urlpatterns += staticfiles_urlpatterns()
