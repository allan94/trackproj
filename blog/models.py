from __future__ import unicode_literals

from django.db import models

class Track(models.Model):
    no = models.IntegerField()
    status = models.CharField(max_length=30)
    colour = models.CharField(max_length=30)
    preReset1 = models.IntegerField()
    preReset2 = models.IntegerField()
    reset1 = models.IntegerField()
    reset2 = models.IntegerField()
    input = models.IntegerField()
    name = models.CharField(max_length=30)

class pinLookup(models.Model):
    no = models.IntegerField()
    gpioNo = models.IntegerField()
    relay = models.IntegerField()
    port = models.IntegerField()
    active = models.IntegerField()
    gpioNumber = models.IntegerField()
    gpioActive = models.IntegerField()
    address = models.IntegerField()
    addressHex = models.CharField(max_length=4)
    
    def save(self):
        self.address = 2**((self.no -2) % 8 )
        self.addressHex = hex(self.address)
        super(pinLookup, self).save()