from django import forms
from django.forms import ModelForm

from blog.models import Track

class TrackForm(ModelForm):
    class Meta:
        model = Track
        fields = ['no','status','colour']

        
      