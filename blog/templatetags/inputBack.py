from django import template
from blog.models import pinLookup
from blog.models import Track

register = template.Library()

@register.filter(name='inputBack')
def inputBack(trackNo):
    try:
        # print(track)
        track = Track.objects.get(no=trackNo)
        if(pinLookup.objects.get(no= track.reset1 ).gpioActive == 1 & pinLookup.objects.get(no= track.reset2 ).gpioActive == 1):
            return "grey"
        else:
            return "red"
    except Exception as e:
        return "red"