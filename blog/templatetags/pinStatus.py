from django import template
from blog.models import pinLookup

register = template.Library()

@register.filter(name='pinStatus')
def pinStatus(pinNo):
    try:
        return pinLookup.objects.get(no=pinNo).active
    except:
        return -101