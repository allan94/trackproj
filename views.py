# include <stdio.h>
# include <stdlib.h>
# include <fcntl.h>
# include <linux/i2c-dev.h>
import datetime
import time
from django.shortcuts import render_to_response
from blog.forms import TrackForm
from blog.models import Track
from blog.models import pinLookup
from django.shortcuts import render
# import Adafruit_BBIO.GPIO as GPIO
import os


# from gethubcontrol import MCP23017

# gethubcontrol = MCP23017()



pulseList = {}


def sendSignalToPins(address1, address2, pinPort):
    addressList = {}
    pinlist = pinLookup.objects.all()
    totalAddress = 0
    addressList[address1] = 0
    for pin in pinlist.filter(port=pinPort):
        if (pin.active == 1):
            print(str(pin.address) + " " + str(pin.no))
            addressList[address1] = addressList[address1] + pin.address

    print("i2cset -y 2 " + address1 + " " + address2 + " 0x" + format(addressList[address1], '02x'))
    os.system("i2cset -y 2 " + address1 + " " + address2 + " 0x" + format(addressList[address1], '02x'))


def reset():

    os.system("i2cset -y 2 0x21 0x00 0x00")
    os.system("i2cset -y 2 0x20 0x00 0x00")
    os.system("i2cset -y 2 0x20 0x12 0x00")
    os.system("i2cset -y 2 0x20 0x13 0x00")
    os.system("i2cset -y 2 0x21 0x13 0x00")
    os.system("i2cset -y 2 0x25 0x00 0x00")
    os.system("i2cset -y 2 0x24 0x00 0x00")
    os.system("i2cset -y 2 0x24 0x12 0x00")
    os.system("i2cset -y 2 0x24 0x13 0x00")
    os.system("i2cset -y 2 0x25 0x13 0x00")
    

    pinlist = pinLookup.objects.all()
    for pin in pinlist:
        pin.active = 0
        pin.save()
    sendSignalToPins("0x20", "0x12", 1)
    sendSignalToPins("0x20", "0x12", 2)
    for trackNumber in pulseList:
        if pulseList[trackNumber] == 1:
            pulseList[trackNumber] = 0

    pinlist = pinLookup.objects.all()
    for pin in pinlist:
        pin.active = 0
        pin.save()
    sendSignalToPins("0x21", "0x12", 1)
    sendSignalToPins("0x21", "0x12", 2)
    for trackNumber in pulseList:
        if pulseList[trackNumber] == 1:
            pulseList[trackNumber] = 0
            
    pinlist = pinLookup.objects.all()
    for pin in pinlist:
        pin.active = 0
        pin.save()
    sendSignalToPins("0x24", "0x12", 1)
    sendSignalToPins("0x24", "0x12", 2)
    for trackNumber in pulseList:
        if pulseList[trackNumber] == 1:
            pulseList[trackNumber] = 0
            
    pinlist = pinLookup.objects.all()
    for pin in pinlist:
        pin.active = 0
        pin.save()
    sendSignalToPins("0x25", "0x12", 1)
    sendSignalToPins("0x25", "0x12", 2)
    for trackNumber in pulseList:
        if pulseList[trackNumber] == 1:
            pulseList[trackNumber] = 0
            
def togglePin(request):
    print("toggleing pin")
    pinNumber = request.POST.get("pin", "")
    pinNumber2 = request.POST.get("pin2", "")
    trackNumber = request.POST.get("trackNo", "")
    p = pinLookup.objects.get(no=pinNumber)
    print("TRACK IS  " + str(trackNumber) + " partititon:  pin number :" + str(pinNumber))

    if pinNumber not in pulseList:
        pulseList[pinNumber] = 0

    if pinNumber2 != '' and pinNumber2 not in pulseList:
        pulseList[pinNumber2] = 0

    if (pinNumber in pulseList and pulseList[pinNumber] == 1):
        pulseList[pinNumber] = 0
        print("turning off pulsing " + pinNumber)
        p.active = 0
        p.save()
        if (pinNumber2 in pulseList and pulseList[pinNumber2] == 1):
            p2 = pinLookup.objects.get(no=pinNumber2)
            pulseList[pinNumber2] = 0
            p2.active = 0
            p2.save()

    else:
        pulseList[pinNumber] = 1
        print("turning on pulsing " + pinNumber)
        p.active = 1
        p.save()
        if (pinNumber2 in pulseList and pulseList[pinNumber2] == 0):
            p2 = pinLookup.objects.get(no=pinNumber2)
            pulseList[pinNumber2] = 1
            p2.active = 1
            p2.save()

    configureSingal(trackNumber, p.port)

    return render(request, 'data.html')


def trackInput(request):
    form = TrackForm()
    tracks = Track.objects.all()
    return render_to_response(request, 'trackInput.html', {'tracks': tracks}, {'form': form})


def configureSingal(trackNumber, pinPort):
    if (int(trackNumber) == 1 or int(trackNumber) == 2):
        sendSignalToPins("0x20", "0X12", pinPort)

    if (int(trackNumber) == 3 or int(trackNumber) == 4):
        sendSignalToPins("0x20", "0X13", pinPort)

    if (int(trackNumber) == 5 or int(trackNumber) == 6):
        sendSignalToPins("0x21", "0X12", pinPort)

    if (int(trackNumber) == 7 or int(trackNumber) == 8):
        sendSignalToPins("0x21", "0X13", pinPort)

    if (int(trackNumber) == 9 or int(trackNumber) == 10):
        sendSignalToPins("0x24", "0X12", pinPort)

    if (int(trackNumber) == 11 or int(trackNumber) == 12):
        sendSignalToPins("0x24", "0X13", pinPort)

    if (int(trackNumber) == 13 or int(trackNumber) == 14):
        sendSignalToPins("0x25", "0X12", pinPort)

    if (int(trackNumber) == 15 or int(trackNumber) == 16):
        sendSignalToPins("0x25", "0X13", pinPort)

def turnOnpulsePin(request):
    print("pulsing pin")
    pinNumber = request.POST.get("pin", "")
    pinNumber2 = request.POST.get("pin2", "")
    trackNumber = request.POST.get("trackNo", "")
    p = pinLookup.objects.get(no=pinNumber)
    print("TRACK IS  " + str(trackNumber) + " partititon:  pin number :" + str(pinNumber))

    if pinNumber not in pulseList:
        pulseList[pinNumber] = 0

    if pinNumber2 != '' and pinNumber2 not in pulseList:
        pulseList[pinNumber2] = 0

    if pulseList[pinNumber] == 0:
        p = pinLookup.objects.get(no=pinNumber)
        p.active = 1
        p.save()
        pulseList[pinNumber] = 1

        if (pinNumber2 in pulseList and pulseList[pinNumber2] == 0):
            p2 = pinLookup.objects.get(no=pinNumber2)
            pulseList[pinNumber2] = 1
            p2.active = 1
            p2.save()

        print("pulsing on " + pinNumber + " state:" + str(p.active))
        configureSingal(trackNumber, p.port)

        time.sleep(1)
        p.active = 0
        p.save()

        if (pinNumber2 in pulseList and pulseList[pinNumber2] == 1):
            p2 = pinLookup.objects.get(no=pinNumber2)
            pulseList[pinNumber2] = 0
            p2.active = 0
            p2.save()

        print("pulsing off " + pinNumber + " state:" + str(p.active))
        time.sleep(1)
        configureSingal(trackNumber, p.port)
        pulseList[pinNumber] = 0

    return render(request, 'data.html')


def data(request):
    reset()
    form = TrackForm()
    tracks = Track.objects.all()
    return render(request, 'data.html', {'tracks': tracks}, {'form': form})


def tracks(request):
    reset()
    form = TrackForm()
    tracks = Track.objects.all()
    return render(request, 'tracks.html', {'tracks': tracks}, {'form': form})


# def checkForInput():
#     # This is where we should put the code to check for GPIO input, and subsequently get that pin and update the gpioActive to 1
#     # i.e pin = pinLookup.objects.get(gpioNo=**gpio Number that has been set to high/low **)
#     pinlist = pinLookup.objects.all()
#     for pin in pinlist:
#         isGPIOPinHigh(pin)
#     return
#
# def isGPIOPinHigh(pin):
#     if  GPIO.input("P8_"+pin.gpioNo):
#         print(str(pin.gpioNo)+ " is HIGH")
#         pin.gpioActive=1
#     else:
#         print(str(pin.gpioNo)+ "LOW")
#         pin.gpioActive = 0
#
def gpioSetup():
    pinlist = pinLookup.objects.all()
#     # for pin in pinlist:
#         # GPIO.setup(pin,gpioNo, GPIO.IN)
#
def loadTracks(request):
#     checkForInput()
    form = TrackForm()
    tracks = Track.objects.all()
    return render(request, 'tracks.html', {'tracks': tracks}, {'form': form})
